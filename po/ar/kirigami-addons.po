# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kirigami-addons package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kirigami-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:45+0000\n"
"PO-Revision-Date: 2022-08-23 19:53+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: Arabic <kde-l10n-ar@kde.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.07.70\n"

#: dateandtime/DatePicker.qml:141
#, kde-format
msgctxt "%1 is month name, %2 is year"
msgid "%1 %2"
msgstr "%1 %2"

#: dateandtime/DatePicker.qml:182
#, fuzzy, kde-format
#| msgid "Days"
msgctxt "kirigami-addons"
msgid "Days"
msgstr "الأيام"

#: dateandtime/DatePicker.qml:190
#, fuzzy, kde-format
#| msgid "Months"
msgctxt "kirigami-addons"
msgid "Months"
msgstr "الأشهر"

#: dateandtime/DatePicker.qml:196
#, fuzzy, kde-format
#| msgid "Years"
msgctxt "kirigami-addons"
msgid "Years"
msgstr "السنوات"

#: dateandtime/DatePopup.qml:55
#, kde-format
msgid "Previous"
msgstr "السابق"

#: dateandtime/DatePopup.qml:64
#, kde-format
msgid "Today"
msgstr "اليوم"

#: dateandtime/DatePopup.qml:73
#, kde-format
msgid "Next"
msgstr "التالي"

#: dateandtime/DatePopup.qml:92
#, kde-format
msgid "Cancel"
msgstr "ألغِ"

#: dateandtime/DatePopup.qml:101
#, kde-format
msgid "Accept"
msgstr "اقبل"

#: dateandtime/private/TumblerTimePicker.qml:88
#, kde-format
msgctxt "Time separator"
msgid ":"
msgstr ""
